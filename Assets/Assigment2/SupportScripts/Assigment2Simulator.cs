﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Assigment2Simulator : MonoBehaviour {

	public Transform ball;

	public Transform target;

	public class RunningExperiment {
		public ThrowExperiment experiment;
		public Vector3 proposedInitialVelocity;
		public float time;
		public bool experimentOver;
	}

	public List<ThrowExperiment> experiments {
		get {
			return ThrowExperiments.instance.experiments;
		}
	}
	
	protected int currentExperimentIndex = 0;
	protected RunningExperiment runningExperiment;

	void OnGUI() {
		if (GUI.Button(new Rect(5,5,150,50),"Start Experiment (" + currentExperimentIndex + ")")) {
			StartExperiment(experiments[currentExperimentIndex]);
		}
		if (GUI.Button(new Rect(160,5,150,50),"Next Experiment")) {
			currentExperimentIndex = (currentExperimentIndex + 1) % experiments.Count;
			runningExperiment = null;
			var exp = ThrowExperiments.instance.experiments[currentExperimentIndex];
			SetTargetAndBall(exp.ballPosition, exp.targetPosition, exp.ballRadius);
		}
		if (runningExperiment != null) {
			GUI.Label(new Rect(5,60,450,20), "Your Solution: V" + runningExperiment.proposedInitialVelocity + " Correct Solution: V" + runningExperiment.experiment.solution);
			var isPassed = Vector3.Distance(runningExperiment.experiment.solution, runningExperiment.proposedInitialVelocity) <= .01f;
			GUI.Label(new Rect(5,80,450,20), "Your solution is: " + (isPassed ? "CORRECT" : "NOT CORRECT"));
		}
	}

	void Awake() {
		var exp = ThrowExperiments.instance.experiments[currentExperimentIndex];
		SetTargetAndBall(exp.ballPosition, exp.targetPosition, exp.ballRadius);
	}

	void SetTargetAndBall(Vector3 ballPosition, Vector3 targetPosition, float ballRadius) {
		ball.position = ballPosition;
		ball.localScale = Vector3.one * ballRadius * 2;
		
		target.position = targetPosition;
		var targetToBallHorizontal = ballPosition - targetPosition;
		targetToBallHorizontal.y = 0;
		target.rotation =  Quaternion.LookRotation(targetToBallHorizontal, Vector3.up) * Quaternion.AngleAxis(90, Vector3.left);
	}

	void StartExperiment(ThrowExperiment exp) {
		SetTargetAndBall(exp.ballPosition, exp.targetPosition, exp.ballRadius);
		runningExperiment = new RunningExperiment() {
			experiment = exp,
			proposedInitialVelocity = BallAim.InitialVelocity(exp.ballPosition, 
			                                                  exp.ballRadius, 
			                                                  exp.targetPosition,
			                                                  exp.gravity,
			                                                  exp.flightTime)
		};
		SetBallPosition(runningExperiment.experiment, runningExperiment.proposedInitialVelocity, runningExperiment.time);
	}

	void SetBallPosition(ThrowExperiment ex, Vector3 initialVelocity, float time) {
		var ballPos = (time * time * .5f * ex.gravity) * Vector3.up + initialVelocity * time + ex.ballPosition;
		ball.position = ballPos;
	}

	void Update() {
		if (runningExperiment == null
		    || runningExperiment.experimentOver) {
			return;
		}
		var ex = runningExperiment.experiment;
		runningExperiment.time += Time.deltaTime;
		SetBallPosition(runningExperiment.experiment, runningExperiment.proposedInitialVelocity, runningExperiment.time);

		if (ball.position.y <= ex.ballRadius) {
			runningExperiment.experimentOver = true;
		}

		var ballToTargetHorizontal = ex.targetPosition - ex.ballPosition;
		var ballHorizontalPos = ball.transform.position - ex.ballPosition;
		ballToTargetHorizontal.y = ballHorizontalPos.y = 0;
		var horizontalDistanceToTarget = Vector3.Dot (ballToTargetHorizontal.normalized, ballHorizontalPos);
		var intrusion = horizontalDistanceToTarget + ex.ballRadius - ballToTargetHorizontal.magnitude;
		if (intrusion >= 0) {
			var horizontalVel = runningExperiment.proposedInitialVelocity;
			horizontalVel.y = 0;
			var intrusionTime = intrusion / horizontalVel.magnitude;
			runningExperiment.time -= intrusionTime;
			SetBallPosition(runningExperiment.experiment, runningExperiment.proposedInitialVelocity, runningExperiment.time);
			runningExperiment.experimentOver = true;
		}

	}

}
