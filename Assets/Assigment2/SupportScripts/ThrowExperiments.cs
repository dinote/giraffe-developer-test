﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ThrowExperiment {
	public Vector3 ballPosition;
	public float ballRadius;
	public Vector3 targetPosition;
	public float flightTime;
	public float gravity;
	public Vector3 solution;
}

public class ThrowExperiments : ScriptableObjectSingleton<ThrowExperiments> {

	public List<ThrowExperiment> experiments;

}
