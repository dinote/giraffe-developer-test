﻿using UnityEngine;
using System.Collections;

public class BallAim : MonoBehaviour {

	/// <summary>
	/// Returns the initial velocity that the ball needs to be thrown with to hit the center of the target
	/// Target is always rotated so that it is facing the target horizontally
	/// Remember that ball has a radius, it is not a point object
	/// </summary>
	/// <returns>The velocity that the ball needs to be thrown to hit the center of the target</returns>
	/// <param name="ballPosition">Initial Ball position.</param>
	/// <param name="ballRadius">Ball radius.</param>
	/// <param name="targetPosition">Target position.</param>
	/// <param name="gravity">Gravity.</param>
	/// <param name="desiredTimeOfFlight">Desired time of flight.</param>
	public static Vector3 InitialVelocity(Vector3 ballPosition, float ballRadius, Vector3 targetPosition, float gravity, float desiredTimeOfFlight) {
		//TODO Implement the solution here
		return (targetPosition - ballPosition) * 2f;
	}
}
