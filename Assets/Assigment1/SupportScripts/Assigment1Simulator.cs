﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Assigment1Simulator : MonoBehaviour {

	public Transform redBallTransform;
	public Transform blueBallTransform;


	public class RunningExperiment {
		public CollisionExperiment experiment;
		public float time;
		public float predictedHitTime;
		public float measuredHitTime;
		public float prevDistance;
		public bool experimentOver;
		public bool predictedCorrect;
		public string result;
	}

	
	public List<CollisionExperiment> experiments {
		get {
			return CollisionExperiments.instance.experiments;
		}
	}

	protected int currentExperimentIndex = 0;
	protected RunningExperiment runningExperiment;



	void OnGUI() {
		if (GUI.Button(new Rect(5,5,200,50),"Start Experiment (" + currentExperimentIndex + ")")) {
			StartExperiment(experiments[currentExperimentIndex]);
		}
		if (GUI.Button(new Rect(210,5,200,50),"Next Experiment")) {
			currentExperimentIndex = (currentExperimentIndex + 1) % experiments.Count;
			runningExperiment = null;
			SetBalls (experiments [currentExperimentIndex]);
		}
		if (runningExperiment != null) {
			GUI.Label(new Rect(5,60,450,20), "Your Solution: " + runningExperiment.predictedHitTime + " Correct Solution: " + runningExperiment.experiment.solution + " time: " + runningExperiment.time);
			if (runningExperiment.experimentOver) {
				bool isPassed = runningExperiment.predictedCorrect;
				GUI.Label(new Rect(5,80,450,20), "Your solution is: " + (isPassed ? "CORRECT" : "NOT CORRECT"));
			}
		}
	}

	void Awake() {
		SetBalls (experiments [0]);
	}

	void SetBalls(CollisionExperiment experiment) {
		redBallTransform.localScale = Vector3.one * experiment.redBall.radius * 2;
		blueBallTransform.localScale = Vector3.one * experiment.blueBall.radius * 2;
		SetBallPositions(experiment, 0);
	}

	void StartExperiment(CollisionExperiment experiment) {

		runningExperiment = new RunningExperiment() {
			experiment = experiment,
			predictedHitTime = CollisionDetector.TimeToCollision(experiment.redBall, experiment.blueBall)
		};
		SetBalls (experiment);
		runningExperiment.prevDistance = Vector3.Distance(redBallTransform.position, blueBallTransform.position);


	}

	void SetBallPositions(CollisionExperiment ex, float time) {
		redBallTransform.position = ex.redBall.initialPosition + time * ex.redBall.velocity;
		blueBallTransform.position = ex.blueBall.initialPosition + time * ex.blueBall.velocity;;
	}

	void Update () {
		if (runningExperiment == null
		    || runningExperiment.experimentOver) {
			return;
		}
		var ex = runningExperiment.experiment;
		runningExperiment.time += Time.deltaTime;
		SetBallPositions(ex, runningExperiment.time);

		var curDistance = Vector3.Distance(redBallTransform.position, blueBallTransform.position);
		var touchDistance = (ex.blueBall.radius + ex.redBall.radius); 
		if (curDistance <= touchDistance) {
			//HIT
			runningExperiment.experimentOver = true;
			runningExperiment.predictedCorrect = runningExperiment.experiment.solution == runningExperiment.predictedHitTime;
			runningExperiment.result = "BALLS HIT";
		} else if (curDistance > runningExperiment.prevDistance
		           && curDistance > 2 * touchDistance) {
			//MISS
			runningExperiment.experimentOver = true;
			runningExperiment.predictedCorrect = runningExperiment.predictedHitTime < 0;
			runningExperiment.result = "BALLS MISS";
		}
		runningExperiment.prevDistance = curDistance;
	}
}
