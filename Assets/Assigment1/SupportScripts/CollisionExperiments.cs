﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CollisionExperiment {
	public Ball redBall;
	public Ball blueBall;
	public float solution;
}

public class ScriptableObjectSingleton<T> : ScriptableObject where T : class{
	static T _instance;
	public static T instance {
		get {
			if (_instance == null) {
				_instance = Resources.Load(typeof(T).Name, typeof(T)) as T;
			}
			return _instance;
		}
	}

}

public class CollisionExperiments : ScriptableObjectSingleton<CollisionExperiments> {

	public List<CollisionExperiment> experiments;


}
