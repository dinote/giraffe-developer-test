﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Ball {
	public Vector3 initialPosition;
	public Vector3 velocity;
	public float radius;
}

public class CollisionDetector {

	/// <summary>
	/// Returns the time of the collision in seconds or value less than 0 if the balls will not collide
	/// </summary>
	/// <returns>time of the collision in seconds or value less than 0 if the balls will not collide</returns>
	public static float TimeToCollision(Ball redBall, Ball blueBall) {
		//TODO Implement the solution here
		return 1000f;
	}
}
